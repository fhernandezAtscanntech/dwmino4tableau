#!/bin/sh
cd `dirname $0`
ROOT_PATH=`pwd`
java -Xms256M -Xmx1024M -cp .:$ROOT_PATH:$ROOT_PATH/../lib/routines.jar:$ROOT_PATH/../lib/dom4j-1.6.1.jar:$ROOT_PATH/../lib/log4j-1.2.16.jar:$ROOT_PATH/../lib/ojdbc14-9i.jar:$ROOT_PATH/../lib/talendcsv.jar:$ROOT_PATH/extraccioncomprasminmesbd_clasificaarticulos_0_1.jar: dwmino4tableau.extraccioncomprasminmesbd_clasificaarticulos_0_1.ExtraccionComprasMinMesBD_ClasificaArticulos  "$@" 